using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class IslandManager : MonoBehaviour
{

    private IslandManager _instance;
    private static IslandManager Instance;


    [Header("Components")]

    [SerializeField]
    private Button _buildPathButton;

    [SerializeField]
    private Transform pathContainer;

    [SerializeField]
    private List<Room> _roomPrefabs;

    [SerializeField]
    private List<Path> _pathPrefabs;

    [Header("Island parameters")]

    [SerializeField]
    [Range(1, 100)]
    private int _level = 1;

    [SerializeField]
    [Range(0f, .99f)]
    private float _pathDensity; // branches direction

    [SerializeField]
    [Range(0f, .99f)]
    private float _pathSplitness; // branches quantity

    [SerializeField]
    private AnimationCurve _pathLengthCurve;


    private Dictionary<DirectionEnum, Vector3> _directions;

    private static Vector3 _startLocation = Vector3.zero;

    private List<Path> _paths;
    private List<Room> _rooms;


    private void Awake()
    {
        _paths = new List<Path>();

        _directions = new Dictionary<DirectionEnum, Vector3>();
        _directions.Add(DirectionEnum.FORWARD, Vector3.forward);
        _directions.Add(DirectionEnum.RIGHT, Vector3.right);
        _directions.Add(DirectionEnum.BACKWARD, Vector3.back);
        _directions.Add(DirectionEnum.LEFT, Vector3.left);

        _buildPathButton.onClick.AddListener(() => BuildPath());
    }

    private void BuildPath()
    {
        RemovePath();
        BuildPath(GetLengthPerLevel(), Vector3.zero, Vector3.forward);
    }

    private void RemovePath()
    {
        foreach (Path p in _paths)
        {
            Destroy(p.gameObject);
        }

        _paths.Clear();
    }

    private int BuildPath(int pathLenght, Vector3 buildLocation, Vector3 buildDirection)
    {
        while (pathLenght > 0)
        {
            // Instantiate path
            int length = UnityEngine.Random.Range(1, 3);
            Path path = InstantiatePath(buildLocation, buildDirection, length);
            buildLocation = path.EndLocation;
            pathLenght -= length;

            // Check directions
            List<Vector3> possibleDirections = GetPosibleDirections(path);
            if (possibleDirections.Count == 0)
                break;

            // Update path direction
            buildDirection = possibleDirections[UnityEngine.Random.Range(0, possibleDirections.Count)];
            possibleDirections.Remove(buildDirection);

            // Check split chance on posible directions
            foreach (Vector3 direction in possibleDirections)
            {
                if (CheckSplitChance())
                {
                    // Split to subpath
                    int newPathLenght = Mathf.Clamp(UnityEngine.Random.Range(1, 5), 1, pathLenght);
                    pathLenght += BuildPath(newPathLenght, buildLocation, direction) - newPathLenght;
                }
            }
        }

        return pathLenght;
    }

    private bool CheckSplitChance()
    {
        return UnityEngine.Random.Range(0f, 1f) < _pathSplitness;
    }

    private Path InstantiatePath(Vector3 buildLocation, Vector3 buildDirection, int length = 1)
    {
        Path newPath = Instantiate(_pathPrefabs[0], buildLocation, Quaternion.LookRotation(buildDirection).normalized, pathContainer);
        newPath.Initialize(_level, buildLocation, length);
        _paths.Add(newPath);
        return newPath;
    }

    private List<Vector3> GetPosibleDirections(Path fromPath)
    {
        List<Vector3> possibles = new List<Vector3>();

        foreach (Vector3 direction in _directions.Values)
        {
            if (Vector3.Equals(direction, - fromPath.transform.forward)) continue;

            // Check is valid location
            if (IsValidLocation(fromPath.EndLocation, direction))
                possibles.Add(direction);
        }

        return possibles;
    }

    private bool IsValidLocation(Vector3 loc, Vector3 dir)
    {
        foreach (Path path in _paths)
        {
            // Check nodes
            if (path.Nodes.Contains(loc))
            {
                if (Vector3.Equals(path.Location, loc) && Vector3.Equals(path.transform.forward, dir))
                    return false;

                if (Vector3.Equals(path.transform.forward, -dir))
                    return false;
            }
        }

        return true;
    }

    private int GetLengthPerLevel()
    {
        return (int) (_pathLengthCurve.Evaluate(_level) / 5) * 5;
    }

}

[Serializable]
public class RangeParameter
{
    [SerializeField]
    private AnimationCurve _min;

    [SerializeField]
    private AnimationCurve _max;
    
    public AnimationCurve Min => _min;
    public AnimationCurve Max => _max;
}

[CustomPropertyDrawer(typeof(ReadOnlyAttribute))]
public class ReadOnlyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position,
                               SerializedProperty property,
                               GUIContent label)
    {
        GUI.enabled = false;
        EditorGUI.PropertyField(position, property, label, true);
        GUI.enabled = true;
    }
}

public class ReadOnlyAttribute : PropertyAttribute
{

}

public abstract class Step : MonoBehaviour
{
    protected List<Vector3> _nodes;
    protected int _level;
    protected int _length;

    public Vector3 Location => _nodes[0];
    public Vector3 EndLocation => _nodes[_nodes.Count-1];
    public List<Vector3> Nodes => _nodes;
    public int Length => _length;
    public int Level => _level;
}

public enum DirectionEnum
{
    FORWARD, RIGHT, BACKWARD, LEFT
}