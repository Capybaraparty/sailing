using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : Step
{
    internal void Initialize(int level, Vector3 location, int length = 1)
    {
        _level = level;
        _length = length;

        // Add Nodes
        InitializeNodes(location);

        // Scale
        Vector3 scale = transform.localScale;
        scale.z = 50 * length;
        transform.localScale = scale;
    }

    private void InitializeNodes(Vector3 location)
    {
        _nodes = new List<Vector3>();

        for (int i = 0; i <= Length; i++)
        {
            Vector3 node = location + i * 50 * transform.forward;
            _nodes.Add(node);
        }
    }
}
